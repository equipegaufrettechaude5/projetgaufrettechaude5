﻿# projetGaufretteChaude5

membres:
    Clarisse Dué
    Loïc Trochon
    Baptiste Roussel
    Hugo Daveluy
    Lucas Marot
    Zacharie Dupuis
    Florian Billault
    Théo Degrieck
    Henri Vanlemmens

OBJECTIF DU SERVICE :

Création d’un service de gestion et de reporting (par groupe) de notes (une matière) :

Donc une note par élève à la fin : la note du semestre de l’élève dans la matière

Les notes saisies sont des intervalles représentée par des lettres : exemple : A= [20 16] 

Les labels des intervalles est libres ainsi que les intervalles associés 

Voir les calculs par intervalles : https://fr.wikipedia.org/wiki/Arithm%C3%A9tique_d%27intervalles

Chaque note intermédiaire à : une date, un nom, un coef 

Le reporting est un fichier PDF 

Le nom du groupe, le correcteur, la date d’édition 

La table des notes (lettres) et la note finale (nombre) 

Des statistiques élémentaires du groupe : moyenne, écart-type, médiane 

Un histogramme de distribution des notes finales (10 bins par défaut)



CONTRAINTES :
Environnement Google : Sheets, Forms, Doc, ... (Macro (Sheets), Script, ...) 

Le SERVICE se lance à partir de Google Sheets. 



LIVRABLES :
Le dossier d’analyse complet (3/4) 

Le programme (1/4) 

Un « user Guide » Simple.
